norm = [
	//ordre important, les séquences de lettres doivent être avant les séquences uniques (pas que mais notamment h)
	{
	  "tableau": ['آ'],
	  "correspondance": 'اا'
	},
	{
	  "tableau": ['ǟ','ā', 'ä','ئ','ء','ى','أ','a','2','A'],
	  "correspondance": 'ا'
	},
	{
		"tableau": ['đ̣','Z'],
		"correspondance": "ظ"
	}
	,
	{
		"tableau": ['th','ŧ'],
		"correspondance": "ث"
	},
	{
		"tableau": ['dh','đ'],
		"correspondance": "ذ"
	},
	{
		"tableau": ['gh','Gh','8','ġ'],
		"correspondance": "غ"
	},
	{
		"tableau": ['kh','Kh','5','x'],
		"correspondance": "خ"
	},
	{
		"tableau": ['sh','ch','Ch','š'],
		"correspondance": "ش"
	},
	{
		"tableau": ['ū', 'ō','ou','Ou','OU','oU','w', 'u','o','U','O','W'],
		"correspondance": "و"
	},
	{
		"tableau": ['r','R','ṛ'],
		"correspondance": "ر"
	},
	{
		"tableau": ['ī','y','Y','i','I'],
		"correspondance": "ي"
	},
	{
		"tableau": ['ḅ', 'b','B'],
		"correspondance": "ب"
	},
	{
		"tableau": ['ε','3','e','E'],
		"correspondance": "ع"
	},
	{
		"tableau": ['j','J','ǧ', 'ž'],
		"correspondance": "ج"
	},
	{
		"tableau": ['ẓ','z'],
		"correspondance": "ز"
	},
	{
		"tableau": ['ṣ','S'],
		"correspondance": "ص"
	},
	
	{
		"tableau": ['ḍ','D'],
		"correspondance": "ض"
	},
	{
		"tableau": ['ṭ','T'],
		"correspondance": "ط"
	},
	{
		"tableau": ['f','F'],
		"correspondance": "ف"
	},
	{
		"tableau": ['č','k','K'],
		"correspondance": "ك"
	},
	{
		"tableau": ['ḷ','l','L'],
		"correspondance": "ل"
	},
	{
		"tableau": ['ṃ','m','M'],
		"correspondance": "م"
	},
	{
		"tableau": ['n','N'],
		"correspondance": "ن"
	},
	{
	  "tableau": ['ڨ','g','G','q','Q','9'],
	  "correspondance": "ق"
	},
	{
		"tableau": ['ḥ','H','7'],
		"correspondance": "ح"
	},
	{
		"tableau": ['d'],
		"correspondance": "د"
	},
	{
		"tableau": ['s'],
		"correspondance": "س"
	},
	{
		"tableau": ['t'],
		"correspondance": "ت"
	},
	{
		"tableau": ['ة','h'],
		"correspondance": "ه"
	}
      ]