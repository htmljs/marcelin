racines = [
	{
	    "initiale": "ا",
	    "finale": "ابو"
	},
	{
	    "initiale": "ابي",
	    "finale": "اجر"
	},
	{
	    "initiale": "اجل",
	    "finale": "اخذ"
	},
	{
	    "initiale": "اخر",
	    "finale": "اخر"
	},
	{
	    "initiale": "اخو",
	    "finale": "ادي"
	},
	{
	    "initiale": "اذ",
	    "finale": "ارض"
	},
	{
	    "initiale": "ارط",
	    "finale": "اسف"
	},
	{
	    "initiale": "اسم",
	    "finale": "افاوي"
	},
	{
	    "initiale": "افرم",
	    "finale": "اكل"
	},
	{
	    "initiale": "اكليل",
	    "finale": "الة"
	},
	{
	    "initiale": "الي",
	    "finale": "ام"
	},
	{
	    "initiale": "اما",
	    "finale": "امل"
	},
	{
	    "initiale": "امن",
	    "finale": "امن"
	},
	{
	    "initiale": "امه",
	    "finale": "انف"
	},
	{
	    "initiale": "انكليز",
	    "finale": "اوف"
	},
	{
	    "initiale": "اول",
	    "finale": "اوي"
	},
	{
	    "initiale": "اي",
	    "finale": "ب"
	},
	{
	    "initiale": "ب",
	    "finale": "بارد"
	},
	{
	    "initiale": "باركو",
	    "finale": "باي"
	},
	{
	    "initiale": "بتر",
	    "finale": "بجغط"
	},
	{
	    "initiale": "بجل",
	    "finale": "بحري"
	},
	{
	    "initiale": "بحلڨ",
	    "finale": "بخص"
	},
	{
	    "initiale": "بخل",
	    "finale": "بدر"
	},
	{
	    "initiale": "بدس",
	    "finale": "بدل"
	},
	{
	    "initiale": "بدن",
	    "finale": "بر"
	},
	{
	    "initiale": "برا",
	    "finale": "بربخ"
	},
	{
	    "initiale": "بربر",
	    "finale": "برح"
	},
	{
	    "initiale": "برد",
	    "finale": "برد"
	},
	{
	    "initiale": "بردع",
	    "finale": "برق"
	},
	{
	    "initiale": "برقش",
	    "finale": "برك"
	},
	{
	    "initiale": "بركي",
	    "finale": "برم"
	},
	{
	    "initiale": "برميل",
	    "finale": "بز"
	},
	{
	    "initiale": "بزبز",
	    "finale": "بس"
	},
	{
	    "initiale": "بسبس",
	    "finale": "بشبش"
	},
	{
	    "initiale": "بشر",
	    "finale": "بشل"
	},
	{
	    "initiale": "بشم",
	    "finale": "بطبط"
	},
	{
	    "initiale": "بطع",
	    "finale": "بطل"
	},
	{
	    "initiale": "بطم",
	    "finale": "بطن"
	},
	{
	    "initiale": "بعبس",
	    "finale": "بعد"
	},
	{
	    "initiale": "بعر",
	    "finale": "بغدد"
	},
	{
	    "initiale": "بغر",
	    "finale": "بقر"
	},
	{
	    "initiale": "بقراج",
	    "finale": "بقي"
	},
	{
	    "initiale": "بكبك",
	    "finale": "بكر"
	},
	{
	    "initiale": "بكش",
	    "finale": "بلبز"
	},
	{
	    "initiale": "بلبل",
	    "finale": "بلط"
	},
	{
	    "initiale": "بلغ",
	    "finale": "بلق"
	},
	{
	    "initiale": "بلم",
	    "finale": "بن"
	},
	{
	    "initiale": "بنّ",
	    "finale": "بنك"
	},
	{
	    "initiale": "بني",
	    "finale": "بهتن"
	},
	{
	    "initiale": "بهج",
	    "finale": "بو"
	},
	{
	    "initiale": "بو",
	    "finale": "بو"
	},
	{
	    "initiale": "بو",
	    "finale": "بو"
	},
	{
	    "initiale": "بو",
	    "finale": "بوخ"
	},
	{
	    "initiale": "بودر",
	    "finale": "بوص"
	},
	{
	    "initiale": "بوط",
	    "finale": "بوه"
	},
	{
	    "initiale": "بوهر",
	    "finale": "بيد"
	},
	{
	    "initiale": "بيدع",
	    "finale": "بيع"
	},
	{
	    "initiale": "بيل",
	    "finale": "بين"
	},
	{
	    "initiale": "ت",
	    "finale": "تاتة"
	},
	{
	    "initiale": "تار",
	    "finale": "تجر"
	},
	{
	    "initiale": "تجط",
	    "finale": "ترتر"
	},
	{
	    "initiale": "ترجم",
	    "finale": "ترم"
	},
	{
	    "initiale": "ترن",
	    "finale": "تفات"
	},
	{
	    "initiale": "تفتار",
	    "finale": "تقي"
	},
	{
	    "initiale": "تك",
	    "finale": "تلف"
	},
	{
	    "initiale": "تلق",
	    "finale": "تم"
	},
	{
	    "initiale": "تمتر",
	    "finale": "تهم"
	},
	{
	    "initiale": "توب",
	    "finale": "توغ"
	},
	{
	    "initiale": "توف",
	    "finale": "ث"
	},
	{
	    "initiale": "ثاء",
	    "finale": "ثبت"
	},
	{
	    "initiale": "ثبثب",
	    "finale": "ثقل"
	},
	{
	    "initiale": "ثقة",
	    "finale": "ثلث"
	},
	{
	    "initiale": "ثلج",
	    "finale": "ثني"
	},
	{
	    "initiale": "ثوب",
	    "finale": "ثور"
	},
	{
	    "initiale": "ثوم",
	    "finale": "جبد"
	},
	{
	    "initiale": "جبر",
	    "finale": "جبس"
	},
	{
	    "initiale": "جبل",
	    "finale": "جد"
	},
	{
	    "initiale": "جدب",
	    "finale": "جدل"
	},
	{
	    "initiale": "جدول",
	    "finale": "جر"
	},
	{
	    "initiale": "جرب",
	    "finale": "جرد"
	},
	{
	    "initiale": "جرز",
	    "finale": "جرنوطة"
	},
	{
	    "initiale": "جرنول",
	    "finale": "جري"
	},
	{
	    "initiale": "جز",
	    "finale": "جزل"
	},
	{
	    "initiale": "جزم",
	    "finale": "جعب"
	},
	{
	    "initiale": "جعجع",
	    "finale": "جعلل"
	},
	{
	    "initiale": "جغبط",
	    "finale": "جقن"
	},
	{
	    "initiale": "جل",
	    "finale": "جلد"
	},
	{
	    "initiale": "جلس",
	    "finale": "جلي"
	},
	{
	    "initiale": "جم",
	    "finale": "جمع"
	},
	{
	    "initiale": "جمع",
	    "finale": "جمع"
	},
	{
	    "initiale": "جمك",
	    "finale": "جن"
	},
	{
	    "initiale": "جنب",
	    "finale": "جنح"
	},
	{
	    "initiale": "جند",
	    "finale": "جهد"
	},
	{
	    "initiale": "جهر",
	    "finale": "جهل"
	},
	{
	    "initiale": "جهم",
	    "finale": "جود"
	},
	{
	    "initiale": "جور",
	    "finale": "جوز"
	},
	{
	    "initiale": "جوع",
	    "finale": "جوف"
	},
	{
	    "initiale": "جوق",
	    "finale": "جيب"
	},
	{
	    "initiale": "جيجل",
	    "finale": "ح"
	},
	{
	    "initiale": "حاء",
	    "finale": "حب"
	},
	{
	    "initiale": "حبر",
	    "finale": "حبق"
	},
	{
	    "initiale": "حبك",
	    "finale": "حتم"
	},
	{
	    "initiale": "حث",
	    "finale": "حجب"
	},
	{
	    "initiale": "حجر",
	    "finale": "حجل"
	},
	{
	    "initiale": "حجم",
	    "finale": "حد"
	},
	{
	    "initiale": "حدا",
	    "finale": "حدر"
	},
	{
	    "initiale": "حدرب",
	    "finale": "حر"
	},
	{
	    "initiale": "حرب",
	    "finale": "حرب"
	},
	{
	    "initiale": "حربش",
	    "finale": "حرد"
	},
	{
	    "initiale": "حرز",
	    "finale": "حرش"
	},
	{
	    "initiale": "حرص",
	    "finale": "حرق"
	},
	{
	    "initiale": "حرقس",
	    "finale": "حرم"
	},
	{
	    "initiale": "حرم",
	    "finale": "حرم"
	},
	{
	    "initiale": "حرمل",
	    "finale": "حزق"
	},
	{
	    "initiale": "حزم",
	    "finale": "حس"
	},
	{
	    "initiale": "حسب",
	    "finale": "حسب"
	},
	{
	    "initiale": "حسبل",
	    "finale": "حسن"
	},
	{
	    "initiale": "حسي",
	    "finale": "حسي"
	},
	{
	    "initiale": "حش",
	    "finale": "حشم"
	},
	{
	    "initiale": "حشن",
	    "finale": "حصر"
	},
	{
	    "initiale": "حصف",
	    "finale": "حصل"
	},
	{
	    "initiale": "حصم",
	    "finale": "حضر"
	},
	{
	    "initiale": "حضن",
	    "finale": "حضن"
	},
	{
	    "initiale": "حضي",
	    "finale": "حطم"
	},
	{
	    "initiale": "حظ",
	    "finale": "حفظ"
	},
	{
	    "initiale": "حفل",
	    "finale": "حق"
	},
	{
	    "initiale": "حق",
	    "finale": "حق"
	},
	{
	    "initiale": "حقب",
	    "finale": "حڨن"
	},
	{
	    "initiale": "حك",
	    "finale": "حكم"
	},
	{
	    "initiale": "حكم",
	    "finale": "حكم"
	},
	{
	    "initiale": "حكي",
	    "finale": "حل"
	},
	{
	    "initiale": "حل",
	    "finale": "حل"
	},
	{
	    "initiale": "حل",
	    "finale": "حل"
	},
	{
	    "initiale": "حلب",
	    "finale": "حلف"
	},
	{
	    "initiale": "حلق",
	    "finale": "حلو"
	},
	{
	    "initiale": "حلوش",
	    "finale": "حمد"
	},
	{
	    "initiale": "حمد",
	    "finale": "حمد"
	},
	{
	    "initiale": "حمدل",
	    "finale": "حمر"
	},
	{
	    "initiale": "حمز",
	    "finale": "حمل"
	},
	{
	    "initiale": "حمي",
	    "finale": "حمي"
	},
	{
	    "initiale": "حن",
	    "finale": "حنب"
	},
	{
	    "initiale": "حنبل",
	    "finale": "حنك"
	},
	{
	    "initiale": "حني",
	    "finale": "حور"
	},
	{
	    "initiale": "حوز",
	    "finale": "حوس"
	},
	{
	    "initiale": "حوش",
	    "finale": "حوف"
	},
	{
	    "initiale": "حوفر",
	    "finale": "حول"
	},
	{
	    "initiale": "حول",
	    "finale": "حول"
	},
	{
	    "initiale": "حول",
	    "finale": "حول"
	},
	{
	    "initiale": "حوم",
	    "finale": "حيى"
	},
	{
	    "initiale": "حيث",
	    "finale": "حيحى"
	},
	{
	    "initiale": "حيد",
	    "finale": "حيل"
	},
	{
	    "initiale": "حين",
	    "finale": "خ"
	},
	{
	    "initiale": "خاء",
	    "finale": "خبز"
	},
	{
	    "initiale": "خبش",
	    "finale": "خبل"
	},
	{
	    "initiale": "خبي",
	    "finale": "ختن"
	},
	{
	    "initiale": "خثر",
	    "finale": "خدع"
	},
	{
	    "initiale": "خدم",
	    "finale": "خدم"
	},
	{
	    "initiale": "خذل",
	    "finale": "خرب"
	},
	{
	    "initiale": "خربش",
	    "finale": "خرج"
	},
	{
	    "initiale": "خرج",
	    "finale": "خرج"
	},
	{
	    "initiale": "خرخب",
	    "finale": "خرص"
	},
	{
	    "initiale": "خرط",
	    "finale": "خرق"
	},
	{
	    "initiale": "خرقن",
	    "finale": "خزم"
	},
	{
	    "initiale": "خزن",
	    "finale": "خسر"
	},
	{
	    "initiale": "خسف",
	    "finale": "خشن"
	},
	{
	    "initiale": "خشي",
	    "finale": "خص"
	},
	{
	    "initiale": "خصب",
	    "finale": "خصم"
	},
	{
	    "initiale": "خصي",
	    "finale": "خط"
	},
	{
	    "initiale": "خطا",
	    "finale": "خطب"
	},
	{
	    "initiale": "خطر",
	    "finale": "خطر"
	},
	{
	    "initiale": "خطف",
	    "finale": "خف"
	},
	{
	    "initiale": "خفت",
	    "finale": "خفض"
	},
	{
	    "initiale": "خفق",
	    "finale": "خل"
	},
	{
	    "initiale": "خلب",
	    "finale": "خلد"
	},
	{
	    "initiale": "خلس",
	    "finale": "خلص"
	},
	{
	    "initiale": "خلط",
	    "finale": "خلط"
	},
	{
	    "initiale": "خلع",
	    "finale": "خلع"
	},
	{
	    "initiale": "خلف",
	    "finale": "خلف"
	},
	{
	    "initiale": "خلق",
	    "finale": "خلق"
	},
	{
	    "initiale": "خلو",
	    "finale": "خلو"
	},
	{
	    "initiale": "خلوض",
	    "finale": "خمد"
	},
	{
	    "initiale": "خمر",
	    "finale": "خمس"
	},
	{
	    "initiale": "خمع",
	    "finale": "خنث"
	},
	{
	    "initiale": "خنج",
	    "finale": "خنق"
	},
	{
	    "initiale": "خو",
	    "finale": "خوف"
	},
	{
	    "initiale": "خول",
	    "finale": "خوي"
	},
	{
	    "initiale": "خي",
	    "finale": "خير"
	},
	{
	    "initiale": "خير",
	    "finale": "خير"
	},
	{
	    "initiale": "خيزب",
	    "finale": "خيل"
	},
	{
	    "initiale": "خيم",
	    "finale": "دبر"
	},
	{
	    "initiale": "دبز",
	    "finale": "دبغ"
	},
	{
	    "initiale": "دبك",
	    "finale": "دحمر"
	},
	{
	    "initiale": "دحمس",
	    "finale": "دخل"
	},
	{
	    "initiale": "دخمر",
	    "finale": "درب"
	},
	{
	    "initiale": "دربز",
	    "finale": "دردخ"
	},
	{
	    "initiale": "دردر",
	    "finale": "درق"
	},
	{
	    "initiale": "درقي",
	    "finale": "درك"
	},
	{
	    "initiale": "درن",
	    "finale": "دسر"
	},
	{
	    "initiale": "دسم",
	    "finale": "دعي"
	},
	{
	    "initiale": "دغبج",
	    "finale": "دغر"
	},
	{
	    "initiale": "دغل",
	    "finale": "دفر"
	},
	{
	    "initiale": "دفس",
	    "finale": "دق"
	},
	{
	    "initiale": "دقدق",
	    "finale": "دقن"
	},
	{
	    "initiale": "دك",
	    "finale": "دل"
	},
	{
	    "initiale": "دلب",
	    "finale": "دمر"
	},
	{
	    "initiale": "دمز",
	    "finale": "دنس"
	},
	{
	    "initiale": "دنش",
	    "finale": "دهس"
	},
	{
	    "initiale": "دهش",
	    "finale": "دهو"
	},
	{
	    "initiale": "دوا",
	    "finale": "دور"
	},
	{
	    "initiale": "دور",
	    "finale": "دور"
	},
	{
	    "initiale": "دورجى",
	    "finale": "دوس"
	},
	{
	    "initiale": "دوسه",
	    "finale": "دوم"
	},
	{
	    "initiale": "دوم",
	    "finale": "دون"
	},
	{
	    "initiale": "دونن",
	    "finale": "ديق"
	},
	{
	    "initiale": "ديك",
	    "finale": "ذال"
	},
	{
	    "initiale": "ذالحين",
	    "finale": "ذرع"
	},
	{
	    "initiale": "ذرو",
	    "finale": "ذكر"
	},
	{
	    "initiale": "ذكي",
	    "finale": "ذل"
	},
	{
	    "initiale": "ذلك",
	    "finale": "ذهب"
	},
	{
	    "initiale": "ذهل",
	    "finale": "ذوق"
	},
	{
	    "initiale": "ذوو",
	    "finale": "راس"
	},
	{
	    "initiale": "راش",
	    "finale": "راي"
	},
	{
	    "initiale": "رب",
	    "finale": "رب"
	},
	{
	    "initiale": "ربح",
	    "finale": "ربط"
	},
	{
	    "initiale": "ربط",
	    "finale": "ربط"
	},
	{
	    "initiale": "ربع",
	    "finale": "ربع"
	},
	{
	    "initiale": "ربق",
	    "finale": "رتب"
	},
	{
	    "initiale": "رتج",
	    "finale": "رثي"
	},
	{
	    "initiale": "رج",
	    "finale": "رجع"
	},
	{
	    "initiale": "رجف",
	    "finale": "رجل"
	},
	{
	    "initiale": "رجم",
	    "finale": "رحب"
	},
	{
	    "initiale": "رحرح",
	    "finale": "رحم"
	},
	{
	    "initiale": "رحي",
	    "finale": "رخي"
	},
	{
	    "initiale": "رد",
	    "finale": "رد"
	},
	{
	    "initiale": "ردا",
	    "finale": "ردف"
	},
	{
	    "initiale": "ردم",
	    "finale": "رزق"
	},
	{
	    "initiale": "رزلامة",
	    "finale": "رسخ"
	},
	{
	    "initiale": "رسف",
	    "finale": "رسي"
	},
	{
	    "initiale": "رش",
	    "finale": "رشق"
	},
	{
	    "initiale": "رشم",
	    "finale": "رصد"
	},
	{
	    "initiale": "رصع",
	    "finale": "رصي"
	},
	{
	    "initiale": "رطب",
	    "finale": "رطل"
	},
	{
	    "initiale": "رعب",
	    "finale": "رعي"
	},
	{
	    "initiale": "رغب",
	    "finale": "رفد"
	},
	{
	    "initiale": "رفرف",
	    "finale": "رفع"
	},
	{
	    "initiale": "رفق",
	    "finale": "رق"
	},
	{
	    "initiale": "رقب",
	    "finale": "رقد"
	},
	{
	    "initiale": "رقرق",
	    "finale": "رقل"
	},
	{
	    "initiale": "رقم",
	    "finale": "ركب"
	},
	{
	    "initiale": "ركد",
	    "finale": "ركز"
	},
	{
	    "initiale": "ركش",
	    "finale": "رمد"
	},
	{
	    "initiale": "رمران",
	    "finale": "رمي"
	},
	{
	    "initiale": "رن",
	    "finale": "رهف"
	},
	{
	    "initiale": "رهم",
	    "finale": "روح"
	},
	{
	    "initiale": "روح",
	    "finale": "روح"
	},
	{
	    "initiale": "رود",
	    "finale": "روض"
	},
	{
	    "initiale": "روط",
	    "finale": "روي"
	},
	{
	    "initiale": "روي",
	    "finale": "ريش"
	},
	{
	    "initiale": "ريض",
	    "finale": "زان"
	},
	{
	    "initiale": "زب",
	    "finale": "زبط"
	},
	{
	    "initiale": "زبل",
	    "finale": "زحم"
	},
	{
	    "initiale": "زخرف",
	    "finale": "زرد"
	},
	{
	    "initiale": "زردب",
	    "finale": "زرف"
	},
	{
	    "initiale": "زرفل",
	    "finale": "زروط"
	},
	{
	    "initiale": "زرول",
	    "finale": "زعزع"
	},
	{
	    "initiale": "زعش",
	    "finale": "زغرط"
	},
	{
	    "initiale": "زغز",
	    "finale": "زڨز"
	},
	{
	    "initiale": "زڨزڨ",
	    "finale": "زكي"
	},
	{
	    "initiale": "زل",
	    "finale": "زلف"
	},
	{
	    "initiale": "زلق",
	    "finale": "زمزم"
	},
	{
	    "initiale": "زمزو",
	    "finale": "زنب"
	},
	{
	    "initiale": "زنبج",
	    "finale": "زنزن"
	},
	{
	    "initiale": "زنزو",
	    "finale": "زهر"
	},
	{
	    "initiale": "زهزه",
	    "finale": "زوج"
	},
	{
	    "initiale": "زوخ",
	    "finale": "زور"
	},
	{
	    "initiale": "زوز",
	    "finale": "زوي"
	},
	{
	    "initiale": "زي",
	    "finale": "زيد"
	},
	{
	    "initiale": "زيد",
	    "finale": "زيد"
	},
	{
	    "initiale": "زير",
	    "finale": "زيل"
	},
	{
	    "initiale": "زيلف",
	    "finale": "س"
	},
	{
	    "initiale": "سابادير",
	    "finale": "سب"
	},
	{
	    "initiale": "سبت",
	    "finale": "سبح"
	},
	{
	    "initiale": "سبخ",
	    "finale": "سبق"
	},
	{
	    "initiale": "سبك",
	    "finale": "سبل"
	},
	{
	    "initiale": "سبن",
	    "finale": "ستر"
	},
	{
	    "initiale": "ستف",
	    "finale": "سجل"
	},
	{
	    "initiale": "سجن",
	    "finale": "سحق"
	},
	{
	    "initiale": "سحل",
	    "finale": "سخن"
	},
	{
	    "initiale": "سخو",
	    "finale": "سدل"
	},
	{
	    "initiale": "سدن",
	    "finale": "سرب"
	},
	{
	    "initiale": "سربح",
	    "finale": "سرح"
	},
	{
	    "initiale": "سرد",
	    "finale": "سرف"
	},
	{
	    "initiale": "سرق",
	    "finale": "سطح"
	},
	{
	    "initiale": "سطر",
	    "finale": "سعد"
	},
	{
	    "initiale": "سعر",
	    "finale": "سعف"
	},
	{
	    "initiale": "سعل",
	    "finale": "سفل"
	},
	{
	    "initiale": "سفن",
	    "finale": "سقط"
	},
	{
	    "initiale": "سقع",
	    "finale": "سقف"
	},
	{
	    "initiale": "سقل",
	    "finale": "سك"
	},
	{
	    "initiale": "سكانوط",
	    "finale": "سكم"
	},
	{
	    "initiale": "سكمل",
	    "finale": "سل"
	},
	{
	    "initiale": "سلا",
	    "finale": "سلح"
	},
	{
	    "initiale": "سلح",
	    "finale": "سلطن"
	},
	{
	    "initiale": "سلع",
	    "finale": "سلك"
	},
	{
	    "initiale": "سلم",
	    "finale": "سلم"
	},
	{
	    "initiale": "سلم",
	    "finale": "سلم"
	},
	{
	    "initiale": "سلن",
	    "finale": "سمح"
	},
	{
	    "initiale": "سمحق",
	    "finale": "سمط"
	},
	{
	    "initiale": "سمع",
	    "finale": "سمة"
	},
	{
	    "initiale": "سمهر",
	    "finale": "سن"
	},
	{
	    "initiale": "سنبر",
	    "finale": "سند"
	},
	{
	    "initiale": "سندر",
	    "finale": "سني"
	},
	{
	    "initiale": "سهد",
	    "finale": "سوا"
	},
	{
	    "initiale": "سوب",
	    "finale": "سود"
	},
	{
	    "initiale": "سور",
	    "finale": "سوط"
	},
	{
	    "initiale": "سوع",
	    "finale": "سوق"
	},
	{
	    "initiale": "سوك",
	    "finale": "سوي"
	},
	{
	    "initiale": "سويد",
	    "finale": "سيح"
	},
	{
	    "initiale": "سيخ",
	    "finale": "سيف"
	},
	{
	    "initiale": "سيق",
	    "finale": "ش"
	},
	{
	    "initiale": "شاش",
	    "finale": "شبح"
	},
	{
	    "initiale": "شبر",
	    "finale": "شبع"
	},
	{
	    "initiale": "شبق",
	    "finale": "شبه"
	},
	{
	    "initiale": "شبو",
	    "finale": "شجع"
	},
	{
	    "initiale": "شجي",
	    "finale": "شحن"
	},
	{
	    "initiale": "شخر",
	    "finale": "شد"
	},
	{
	    "initiale": "شدخ",
	    "finale": "شر"
	},
	{
	    "initiale": "شرب",
	    "finale": "شرتل"
	},
	{
	    "initiale": "شرد",
	    "finale": "شرشم"
	},
	{
	    "initiale": "شرط",
	    "finale": "شرع"
	},
	{
	    "initiale": "شرف",
	    "finale": "شرق"
	},
	{
	    "initiale": "شرڨرڨ",
	    "finale": "شرم"
	},
	{
	    "initiale": "شرمط",
	    "finale": "شطب"
	},
	{
	    "initiale": "شطح",
	    "finale": "شع"
	},
	{
	    "initiale": "شعب",
	    "finale": "شعل"
	},
	{
	    "initiale": "شعن",
	    "finale": "شعل"
	},
	{
	    "initiale": "شعنن",
	    "finale": "شفق"
	},
	{
	    "initiale": "شفه",
	    "finale": "شق"
	},
	{
	    "initiale": "شقر",
	    "finale": "شقي"
	},
	{
	    "initiale": "شك",
	    "finale": "شكشك"
	},
	{
	    "initiale": "شكشم",
	    "finale": "شل"
	},
	{
	    "initiale": "شلب",
	    "finale": "شلفخ"
	},
	{
	    "initiale": "شلفط",
	    "finale": "شلي"
	},
	{
	    "initiale": "شم",
	    "finale": "شمس"
	},
	{
	    "initiale": "شمشر",
	    "finale": "شملك"
	},
	{
	    "initiale": "شملل",
	    "finale": "شنف"
	},
	{
	    "initiale": "شنق",
	    "finale": "شهب"
	},
	{
	    "initiale": "شهر",
	    "finale": "شهر"
	},
	{
	    "initiale": "شهراو",
	    "finale": "شوب"
	},
	{
	    "initiale": "شوبر",
	    "finale": "شورب"
	},
	{
	    "initiale": "شوش",
	    "finale": "شوق"
	},
	{
	    "initiale": "شوك",
	    "finale": "شوي"
	},
	{
	    "initiale": "شيا",
	    "finale": "شيخ"
	},
	{
	    "initiale": "شيد",
	    "finale": "شيع"
	},
	{
	    "initiale": "شيغ",
	    "finale": "شيه"
	},
	{
	    "initiale": "ص",
	    "finale": "صبح"
	},
	{
	    "initiale": "صبر",
	    "finale": "صبغ"
	},
	{
	    "initiale": "صبن",
	    "finale": "صح"
	},
	{
	    "initiale": "صح",
	    "finale": "صح"
	},
	{
	    "initiale": "صحب",
	    "finale": "صحف"
	},
	{
	    "initiale": "صحف",
	    "finale": "صدر"
	},
	{
	    "initiale": "صدع",
	    "finale": "صدق"
	},
	{
	    "initiale": "صدم",
	    "finale": "صرح"
	},
	{
	    "initiale": "صرخ",
	    "finale": "صرف"
	},
	{
	    "initiale": "صرفق",
	    "finale": "صعب"
	},
	{
	    "initiale": "صعد",
	    "finale": "صف"
	},
	{
	    "initiale": "صفح",
	    "finale": "صفر"
	},
	{
	    "initiale": "صفصط",
	    "finale": "صفي"
	},
	{
	    "initiale": "صقع",
	    "finale": "صقل"
	},
	{
	    "initiale": "صك",
	    "finale": "صلح"
	},
	{
	    "initiale": "صلح",
	    "finale": "صلح"
	},
	{
	    "initiale": "صلط",
	    "finale": "صمت"
	},
	{
	    "initiale": "صمخ",
	    "finale": "صنع"
	},
	{
	    "initiale": "صنف",
	    "finale": "صوب"
	},
	{
	    "initiale": "صوبر",
	    "finale": "صور"
	},
	{
	    "initiale": "صوع",
	    "finale": "صون"
	},
	{
	    "initiale": "صيب",
	    "finale": "صير"
	},
	{
	    "initiale": "صيش",
	    "finale": "ضان"
	},
	{
	    "initiale": "ضاي",
	    "finale": "ضحك"
	},
	{
	    "initiale": "ضحو",
	    "finale": "ضر"
	},
	{
	    "initiale": "ضرب",
	    "finale": "ضرب"
	},
	{
	    "initiale": "ضرح",
	    "finale": "ضرس"
	},
	{
	    "initiale": "ضرط",
	    "finale": "ضفر"
	},
	{
	    "initiale": "ضل",
	    "finale": "ضمن"
	},
	{
	    "initiale": "ضميض",
	    "finale": "ضوي"
	},
	{
	    "initiale": "ضير",
	    "finale": "ضيق"
	},
	{
	    "initiale": "ضين",
	    "finale": "طب"
	},
	{
	    "initiale": "طبج",
	    "finale": "طبع"
	},
	{
	    "initiale": "طبق",
	    "finale": "طبل"
	},
	{
	    "initiale": "طبم",
	    "finale": "طخطح"
	},
	{
	    "initiale": "طر",
	    "finale": "طرح"
	},
	{
	    "initiale": "طرد",
	    "finale": "طرش"
	},
	{
	    "initiale": "طرشح",
	    "finale": "طرف"
	},
	{
	    "initiale": "طرق",
	    "finale": "طرق"
	},
	{
	    "initiale": "طرم",
	    "finale": "طعن"
	},
	{
	    "initiale": "طغر",
	    "finale": "طقلل"
	},
	{
	    "initiale": "طل",
	    "finale": "طلب"
	},
	{
	    "initiale": "طلح",
	    "finale": "طلع"
	},
	{
	    "initiale": "طلف",
	    "finale": "طلق"
	},
	{
	    "initiale": "طلق",
	    "finale": "طلمس"
	},
	{
	    "initiale": "طلي",
	    "finale": "طمل"
	},
	{
	    "initiale": "طمن",
	    "finale": "طور"
	},
	{
	    "initiale": "طورنو",
	    "finale": "طوع"
	},
	{
	    "initiale": "طوف",
	    "finale": "طول"
	},
	{
	    "initiale": "طوم",
	    "finale": "طيب"
	},
	{
	    "initiale": "طيح",
	    "finale": "طيح"
	},
	{
	    "initiale": "طيح",
	    "finale": "طيح"
	},
	{
	    "initiale": "طير",
	    "finale": "طيش"
	},
	{
	    "initiale": "طيطم",
	    "finale": "ظفر"
	},
	{
	    "initiale": "ظل",
	    "finale": "ظن"
	},
	{
	    "initiale": "ظهر",
	    "finale": "ظهر"
	},
	{
	    "initiale": "ع",
	    "finale": "عبد"
	},
	{
	    "initiale": "عبد",
	    "finale": "عبد"
	},
	{
	    "initiale": "عبر",
	    "finale": "عبس"
	},
	{
	    "initiale": "عبق",
	    "finale": "عتم"
	},
	{
	    "initiale": "عته",
	    "finale": "عجب"
	},
	{
	    "initiale": "عجر",
	    "finale": "عجل"
	},
	{
	    "initiale": "عجم",
	    "finale": "عد"
	},
	{
	    "initiale": "عدس",
	    "finale": "عدل"
	},
	{
	    "initiale": "عدل",
	    "finale": "عدل"
	},
	{
	    "initiale": "عدم",
	    "finale": "عدي"
	},
	{
	    "initiale": "عذب",
	    "finale": "عذر"
	},
	{
	    "initiale": "عر",
	    "finale": "عرج"
	},
	{
	    "initiale": "عرحن",
	    "finale": "عرض"
	},
	{
	    "initiale": "عرعر",
	    "finale": "عرف"
	},
	{
	    "initiale": "عرف",
	    "finale": "عرف"
	},
	{
	    "initiale": "عرق",
	    "finale": "عرق"
	},
	{
	    "initiale": "عرقب",
	    "finale": "عري"
	},
	{
	    "initiale": "عز",
	    "finale": "عز"
	},
	{
	    "initiale": "عزب",
	    "finale": "عزم"
	},
	{
	    "initiale": "عزن",
	    "finale": "عسل"
	},
	{
	    "initiale": "عسلج",
	    "finale": "عشر"
	},
	{
	    "initiale": "عشعش",
	    "finale": "عصب"
	},
	{
	    "initiale": "عصد",
	    "finale": "عصر"
	},
	{
	    "initiale": "عصف",
	    "finale": "عطر"
	},
	{
	    "initiale": "عطس",
	    "finale": "عطل"
	},
	{
	    "initiale": "عطن",
	    "finale": "عظم"
	},
	{
	    "initiale": "عف",
	    "finale": "عفس"
	},
	{
	    "initiale": "عفش",
	    "finale": "عقب"
	},
	{
	    "initiale": "عقد",
	    "finale": "عقد"
	},
	{
	    "initiale": "عقر",
	    "finale": "عقف"
	},
	{
	    "initiale": "عقل",
	    "finale": "عقل"
	},
	{
	    "initiale": "عقم",
	    "finale": "عكس"
	},
	{
	    "initiale": "عكش",
	    "finale": "علف"
	},
	{
	    "initiale": "علق",
	    "finale": "علم"
	},
	{
	    "initiale": "علم",
	    "finale": "علم"
	},
	{
	    "initiale": "علن",
	    "finale": "علو"
	},
	{
	    "initiale": "علون",
	    "finale": "علون"
	},
	{
	    "initiale": "عم",
	    "finale": "عمد"
	},
	{
	    "initiale": "عمر",
	    "finale": "عمر"
	},
	{
	    "initiale": "عمر",
	    "finale": "عمر"
	},
	{
	    "initiale": "عمش",
	    "finale": "عمل"
	},
	{
	    "initiale": "عمن",
	    "finale": "عن"
	},
	{
	    "initiale": "عنّ",
	    "finale": "عند"
	},
	{
	    "initiale": "عنز",
	    "finale": "عنق"
	},
	{
	    "initiale": "عنقد",
	    "finale": "عني"
	},
	{
	    "initiale": "عهد",
	    "finale": "عود"
	},
	{
	    "initiale": "عوذ",
	    "finale": "عور"
	},
	{
	    "initiale": "عوسج",
	    "finale": "عول"
	},
	{
	    "initiale": "عوم",
	    "finale": "عون"
	},
	{
	    "initiale": "عوي",
	    "finale": "عيد"
	},
	{
	    "initiale": "عيدد",
	    "finale": "عيش"
	},
	{
	    "initiale": "عيط",
	    "finale": "عين"
	},
	{
	    "initiale": "عين",
	    "finale": "عين"
	},
	{
	    "initiale": "غ",
	    "finale": "غبط"
	},
	{
	    "initiale": "غبغب",
	    "finale": "غدر"
	},
	{
	    "initiale": "غدمس",
	    "finale": "غرب"
	},
	{
	    "initiale": "غربل",
	    "finale": "غرز"
	},
	{
	    "initiale": "غرس",
	    "finale": "غرف"
	},
	{
	    "initiale": "غرق",
	    "finale": "غرم"
	},
	{
	    "initiale": "غرمل",
	    "finale": "غزل"
	},
	{
	    "initiale": "غزي",
	    "finale": "غش"
	},
	{
	    "initiale": "غشت",
	    "finale": "غصب"
	},
	{
	    "initiale": "غصن",
	    "finale": "غفد"
	},
	{
	    "initiale": "غفر",
	    "finale": "غل"
	},
	{
	    "initiale": "غلب",
	    "finale": "غلث"
	},
	{
	    "initiale": "غلس",
	    "finale": "غلق"
	},
	{
	    "initiale": "غلم",
	    "finale": "غم"
	},
	{
	    "initiale": "غمد",
	    "finale": "غمق"
	},
	{
	    "initiale": "غمل",
	    "finale": "غني"
	},
	{
	    "initiale": "غوبش",
	    "finale": "غور"
	},
	{
	    "initiale": "غور",
	    "finale": "غوي"
	},
	{
	    "initiale": "غي",
	    "finale": "غيب"
	},
	{
	    "initiale": "غيث",
	    "finale": "غير"
	},
	{
	    "initiale": "غيز",
	    "finale": "ف"
	},
	{
	    "initiale": "ف",
	    "finale": "فتح"
	},
	{
	    "initiale": "فتر",
	    "finale": "فتر"
	},
	{
	    "initiale": "فتس",
	    "finale": "فتن"
	},
	{
	    "initiale": "فتو",
	    "finale": "فجر"
	},
	{
	    "initiale": "فجع",
	    "finale": "فحل"
	},
	{
	    "initiale": "فحم",
	    "finale": "فخم"
	},
	{
	    "initiale": "فدح",
	    "finale": "فربن"
	},
	{
	    "initiale": "فرتل",
	    "finale": "فرح"
	},
	{
	    "initiale": "فرخ",
	    "finale": "فرد"
	},
	{
	    "initiale": "فردخ",
	    "finale": "فرس"
	},
	{
	    "initiale": "فرسخ",
	    "finale": "فرض"
	},
	{
	    "initiale": "فرط",
	    "finale": "فرطط"
	},
	{
	    "initiale": "فرع",
	    "finale": "فرفر"
	},
	{
	    "initiale": "فرفش",
	    "finale": "فرق"
	},
	{
	    "initiale": "فرڨط",
	    "finale": "فرم"
	},
	{
	    "initiale": "فرمل",
	    "finale": "فزع"
	},
	{
	    "initiale": "فزن",
	    "finale": "فسد"
	},
	{
	    "initiale": "فسر",
	    "finale": "فسق"
	},
	{
	    "initiale": "فسكر",
	    "finale": "فشل"
	},
	{
	    "initiale": "فشي",
	    "finale": "فصل"
	},
	{
	    "initiale": "فصم",
	    "finale": "فض"
	},
	{
	    "initiale": "فضح",
	    "finale": "فضل"
	},
	{
	    "initiale": "فضي",
	    "finale": "فطم"
	},
	{
	    "initiale": "فطن",
	    "finale": "فقد"
	},
	{
	    "initiale": "فقر",
	    "finale": "فك"
	},
	{
	    "initiale": "فكر",
	    "finale": "فل"
	},
	{
	    "initiale": "فلامبنڨ",
	    "finale": "فلس"
	},
	{
	    "initiale": "فلسف",
	    "finale": "فلق"
	},
	{
	    "initiale": "فلك",
	    "finale": "فندق"
	},
	{
	    "initiale": "فنر",
	    "finale": "فهم"
	},
	{
	    "initiale": "فهي",
	    "finale": "فور"
	},
	{
	    "initiale": "فوز",
	    "finale": "فوق"
	},
	{
	    "initiale": "فول",
	    "finale": "في"
	},
	{
	    "initiale": "فيا",
	    "finale": "فيض"
	},
	{
	    "initiale": "فيضل",
	    "finale": "قاني"
	},
	{
	    "initiale": "قاو",
	    "finale": "قبض"
	},
	{
	    "initiale": "قبط",
	    "finale": "قبل"
	},
	{
	    "initiale": "قبل",
	    "finale": "قبل"
	},
	{
	    "initiale": "قبو",
	    "finale": "قبو"
	},
	{
	    "initiale": "قبوم",
	    "finale": "قحب"
	},
	{
	    "initiale": "قحط",
	    "finale": "قدح"
	},
	{
	    "initiale": "قدر",
	    "finale": "قدر"
	},
	{
	    "initiale": "قدس",
	    "finale": "قدم"
	},
	{
	    "initiale": "قدم",
	    "finale": "قدم"
	},
	{
	    "initiale": "قده",
	    "finale": "قر"
	},
	{
	    "initiale": "قر",
	    "finale": "قر"
	},
	{
	    "initiale": "قرا",
	    "finale": "قرب"
	},
	{
	    "initiale": "قربج",
	    "finale": "قرد"
	},
	{
	    "initiale": "قردش",
	    "finale": "قرش"
	},
	{
	    "initiale": "قرشل",
	    "finale": "قرض"
	},
	{
	    "initiale": "قرط",
	    "finale": "قرع"
	},
	{
	    "initiale": "قرغل",
	    "finale": "قرقش"
	},
	{
	    "initiale": "قرقط",
	    "finale": "قرن"
	},
	{
	    "initiale": "قرن",
	    "finale": "قرنج"
	},
	{
	    "initiale": "قرندلي",
	    "finale": "قزدر"
	},
	{
	    "initiale": "قزم",
	    "finale": "قسط"
	},
	{
	    "initiale": "قسطل",
	    "finale": "قسي"
	},
	{
	    "initiale": "قش",
	    "finale": "قشط"
	},
	{
	    "initiale": "قشع",
	    "finale": "قصب"
	},
	{
	    "initiale": "قصد",
	    "finale": "قصدر"
	},
	{
	    "initiale": "قصر",
	    "finale": "قصع"
	},
	{
	    "initiale": "قصف",
	    "finale": "قضي"
	},
	{
	    "initiale": "قط",
	    "finale": "قطر"
	},
	{
	    "initiale": "قطرم",
	    "finale": "قطع"
	},
	{
	    "initiale": "قطع",
	    "finale": "قطع"
	},
	{
	    "initiale": "قطف",
	    "finale": "قطف"
	},
	{
	    "initiale": "قطقط",
	    "finale": "قعد"
	},
	{
	    "initiale": "قعر",
	    "finale": "قعلص"
	},
	{
	    "initiale": "قعمز",
	    "finale": "قفي"
	},
	{
	    "initiale": "قق",
	    "finale": "قل"
	},
	{
	    "initiale": "قلب",
	    "finale": "قلب"
	},
	{
	    "initiale": "قلب",
	    "finale": "قلب"
	},
	{
	    "initiale": "قلبضن",
	    "finale": "قلش"
	},
	{
	    "initiale": "قلط",
	    "finale": "قلق"
	},
	{
	    "initiale": "قلقل",
	    "finale": "قمبع"
	},
	{
	    "initiale": "قمج",
	    "finale": "قمط"
	},
	{
	    "initiale": "قمطر",
	    "finale": "قنت"
	},
	{
	    "initiale": "قنجر",
	    "finale": "قنطر"
	},
	{
	    "initiale": "قنطس",
	    "finale": "قهر"
	},
	{
	    "initiale": "قهقر",
	    "finale": "قود"
	},
	{
	    "initiale": "قودارم",
	    "finale": "قوس"
	},
	{
	    "initiale": "ڨوسطو",
	    "finale": "قول"
	},
	{
	    "initiale": "قولب",
	    "finale": "قوم"
	},
	{
	    "initiale": "قوم",
	    "finale": "قوم"
	},
	{
	    "initiale": "قومص",
	    "finale": "قومص"
	},
	{
	    "initiale": "ڨومن",
	    "finale": "قيد"
	},
	{
	    "initiale": "قيدح",
	    "finale": "قيل"
	},
	{
	    "initiale": "قيم",
	    "finale": "كاساموشاشو"
	},
	{
	    "initiale": "كاش",
	    "finale": "كبر"
	},
	{
	    "initiale": "كبر",
	    "finale": "كبر"
	},
	{
	    "initiale": "كبرت",
	    "finale": "كتب"
	},
	{
	    "initiale": "كتف",
	    "finale": "كتف"
	},
	{
	    "initiale": "كتل",
	    "finale": "كثر"
	},
	{
	    "initiale": "كثف",
	    "finale": "كحل"
	},
	{
	    "initiale": "كخ",
	    "finale": "كدس"
	},
	{
	    "initiale": "كدي",
	    "finale": "كرب"
	},
	{
	    "initiale": "كربس",
	    "finale": "كرس"
	},
	{
	    "initiale": "كرش",
	    "finale": "كرك"
	},
	{
	    "initiale": "كركب",
	    "finale": "كرم"
	},
	{
	    "initiale": "كرمش",
	    "finale": "كره"
	},
	{
	    "initiale": "كري",
	    "finale": "كسب"
	},
	{
	    "initiale": "كسبر",
	    "finale": "كسر"
	},
	{
	    "initiale": "كسف",
	    "finale": "كسل"
	},
	{
	    "initiale": "كسم",
	    "finale": "كشرد"
	},
	{
	    "initiale": "كشط",
	    "finale": "كعب"
	},
	{
	    "initiale": "كعبر",
	    "finale": "كف"
	},
	{
	    "initiale": "كفا",
	    "finale": "كفي"
	},
	{
	    "initiale": "ككي",
	    "finale": "كل"
	},
	{
	    "initiale": "كلا",
	    "finale": "كلح"
	},
	{
	    "initiale": "كلخ",
	    "finale": "كلف"
	},
	{
	    "initiale": "كلكز",
	    "finale": "كم"
	},
	{
	    "initiale": "كما",
	    "finale": "كمل"
	},
	{
	    "initiale": "كمن",
	    "finale": "كمن"
	},
	{
	    "initiale": "كمنش",
	    "finale": "كنبو"
	},
	{
	    "initiale": "كنت",
	    "finale": "كهن"
	},
	{
	    "initiale": "كهي",
	    "finale": "كوفر"
	},
	{
	    "initiale": "كوكب",
	    "finale": "كون"
	},
	{
	    "initiale": "كوني",
	    "finale": "كيف"
	},
	{
	    "initiale": "كيل",
	    "finale": "ل"
	},
	{
	    "initiale": "لا",
	    "finale": "لان"
	},
	{
	    "initiale": "لاه",
	    "finale": "لبس"
	},
	{
	    "initiale": "لبش",
	    "finale": "لثم"
	},
	{
	    "initiale": "لج",
	    "finale": "لحف"
	},
	{
	    "initiale": "لحق",
	    "finale": "لحن"
	},
	{
	    "initiale": "لحي",
	    "finale": "لذي"
	},
	{
	    "initiale": "لرو",
	    "finale": "لزم"
	},
	{
	    "initiale": "لس",
	    "finale": "لص"
	},
	{
	    "initiale": "لصق",
	    "finale": "لطف"
	},
	{
	    "initiale": "لطم",
	    "finale": "لعن"
	},
	{
	    "initiale": "لغ",
	    "finale": "لف"
	},
	{
	    "initiale": "لفت",
	    "finale": "لقط"
	},
	{
	    "initiale": "لقف",
	    "finale": "لقي"
	},
	{
	    "initiale": "لك",
	    "finale": "لم"
	},
	{
	    "initiale": "لما",
	    "finale": "لنك"
	},
	{
	    "initiale": "لهب",
	    "finale": "لو"
	},
	{
	    "initiale": "لواش",
	    "finale": "لوك"
	},
	{
	    "initiale": "لوكاندة",
	    "finale": "لوي"
	},
	{
	    "initiale": "ليز",
	    "finale": "ليق"
	},
	{
	    "initiale": "ليل",
	    "finale": "لين"
	},
	{
	    "initiale": "م",
	    "finale": "ما"
	},
	{
	    "initiale": "مآ",
	    "finale": "متع"
	},
	{
	    "initiale": "متل",
	    "finale": "مثل"
	},
	{
	    "initiale": "مجد",
	    "finale": "محن"
	},
	{
	    "initiale": "محند",
	    "finale": "مد"
	},
	{
	    "initiale": "مدح",
	    "finale": "مدن"
	},
	{
	    "initiale": "مدي",
	    "finale": "مرا"
	},
	{
	    "initiale": "مربط",
	    "finale": "مرد"
	},
	{
	    "initiale": "مردقوش",
	    "finale": "مرغن"
	},
	{
	    "initiale": "مرق",
	    "finale": "مرمز"
	},
	{
	    "initiale": "مرن",
	    "finale": "مزن"
	},
	{
	    "initiale": "مزهر",
	    "finale": "مسخ"
	},
	{
	    "initiale": "مسخر",
	    "finale": "مسك"
	},
	{
	    "initiale": "مسكن",
	    "finale": "مشي"
	},
	{
	    "initiale": "مص",
	    "finale": "مضي"
	},
	{
	    "initiale": "مطر",
	    "finale": "مع"
	},
	{
	    "initiale": "معجز",
	    "finale": "مقنن"
	},
	{
	    "initiale": "مك",
	    "finale": "مل"
	},
	{
	    "initiale": "ملا",
	    "finale": "ملس"
	},
	{
	    "initiale": "ملص",
	    "finale": "ملك"
	},
	{
	    "initiale": "ملك",
	    "finale": "ملك"
	},
	{
	    "initiale": "ملمز",
	    "finale": "من"
	},
	{
	    "initiale": "منّ",
	    "finale": "منح"
	},
	{
	    "initiale": "مند",
	    "finale": "مني"
	},
	{
	    "initiale": "مهبل",
	    "finale": "مو"
	},
	{
	    "initiale": "موت",
	    "finale": "موز"
	},
	{
	    "initiale": "موزيڨة",
	    "finale": "مون"
	},
	{
	    "initiale": "موه",
	    "finale": "ميز"
	},
	{
	    "initiale": "ميزاب",
	    "finale": "ميل"
	},
	{
	    "initiale": "ميم",
	    "finale": "نبح"
	},
	{
	    "initiale": "نبذ",
	    "finale": "نبه"
	},
	{
	    "initiale": "نبو",
	    "finale": "نتف"
	},
	{
	    "initiale": "نتق",
	    "finale": "نجر"
	},
	{
	    "initiale": "نجز",
	    "finale": "نجو"
	},
	{
	    "initiale": "نح",
	    "finale": "نحو"
	},
	{
	    "initiale": "نحي",
	    "finale": "ندب"
	},
	{
	    "initiale": "ندح",
	    "finale": "نده"
	},
	{
	    "initiale": "ندي",
	    "finale": "نزع"
	},
	{
	    "initiale": "نزغ",
	    "finale": "نزل"
	},
	{
	    "initiale": "نزم",
	    "finale": "نزنز"
	},
	{
	    "initiale": "نزه",
	    "finale": "نسب"
	},
	{
	    "initiale": "نسج",
	    "finale": "نسخ"
	},
	{
	    "initiale": "نسر",
	    "finale": "نسي"
	},
	{
	    "initiale": "نش",
	    "finale": "نشر"
	},
	{
	    "initiale": "نشز",
	    "finale": "نشف"
	},
	{
	    "initiale": "نشق",
	    "finale": "نصب"
	},
	{
	    "initiale": "نصت",
	    "finale": "نصر"
	},
	{
	    "initiale": "نصف",
	    "finale": "نصي"
	},
	{
	    "initiale": "نض",
	    "finale": "نظر"
	},
	{
	    "initiale": "نظر",
	    "finale": "نظر"
	},
	{
	    "initiale": "نظف",
	    "finale": "نعس"
	},
	{
	    "initiale": "نعش",
	    "finale": "نعم"
	},
	{
	    "initiale": "نعنع",
	    "finale": "نفح"
	},
	{
	    "initiale": "نفخ",
	    "finale": "نفذ"
	},
	{
	    "initiale": "نفر",
	    "finale": "نفس"
	},
	{
	    "initiale": "نفس",
	    "finale": "نفس"
	},
	{
	    "initiale": "نفض",
	    "finale": "نفع"
	},
	{
	    "initiale": "نفق",
	    "finale": "نفي"
	},
	{
	    "initiale": "نقب",
	    "finale": "نقر"
	},
	{
	    "initiale": "نڨريلية",
	    "finale": "نقش"
	},
	{
	    "initiale": "نقص",
	    "finale": "نقص"
	},
	{
	    "initiale": "نقض",
	    "finale": "نقل"
	},
	{
	    "initiale": "نقم",
	    "finale": "نقم"
	},
	{
	    "initiale": "نڨمر",
	    "finale": "نكر"
	},
	{
	    "initiale": "نكس",
	    "finale": "نمس"
	},
	{
	    "initiale": "نمسر",
	    "finale": "نهد"
	},
	{
	    "initiale": "نهر",
	    "finale": "نهي"
	},
	{
	    "initiale": "نو",
	    "finale": "نوب"
	},
	{
	    "initiale": "نوح",
	    "finale": "نور"
	},
	{
	    "initiale": "نوس",
	    "finale": "نوض"
	},
	{
	    "initiale": "نوط",
	    "finale": "نوم"
	},
	{
	    "initiale": "نومرو",
	    "finale": "نوي"
	},
	{
	    "initiale": "ني",
	    "finale": "نيف"
	},
	{
	    "initiale": "نيك",
	    "finale": "ه"
	},
	{
	    "initiale": "ها",
	    "finale": "هبش"
	},
	{
	    "initiale": "هبط",
	    "finale": "هتك"
	},
	{
	    "initiale": "هجد",
	    "finale": "هجم"
	},
	{
	    "initiale": "هجن",
	    "finale": "هدرك"
	},
	{
	    "initiale": "هدف",
	    "finale": "هدي"
	},
	{
	    "initiale": "هذا",
	    "finale": "هذر"
	},
	{
	    "initiale": "هر",
	    "finale": "هرش"
	},
	{
	    "initiale": "هرشم",
	    "finale": "هز"
	},
	{
	    "initiale": "هزا",
	    "finale": "هسهس"
	},
	{
	    "initiale": "هش",
	    "finale": "هل"
	},
	{
	    "initiale": "هلّ",
	    "finale": "هم"
	},
	{
	    "initiale": "همّ",
	    "finale": "همز"
	},
	{
	    "initiale": "همسس",
	    "finale": "همل"
	},
	{
	    "initiale": "هملج",
	    "finale": "هند"
	},
	{
	    "initiale": "هندب",
	    "finale": "هوش"
	},
	{
	    "initiale": "هول",
	    "finale": "هوي"
	},
	{
	    "initiale": "هي",
	    "finale": "هيج"
	},
	{
	    "initiale": "هيس",
	    "finale": "و"
	},
	{
	    "initiale": "واد",
	    "finale": "وتر"
	},
	{
	    "initiale": "وتي",
	    "finale": "وجب"
	},
	{
	    "initiale": "وجب",
	    "finale": "وجب"
	},
	{
	    "initiale": "وجد",
	    "finale": "وجد"
	},
	{
	    "initiale": "وجر",
	    "finale": "وجه"
	},
	{
	    "initiale": "وجه",
	    "finale": "وجه"
	},
	{
	    "initiale": "وح",
	    "finale": "وحد"
	},
	{
	    "initiale": "وحش",
	    "finale": "وحش"
	},
	{
	    "initiale": "وحل",
	    "finale": "وخر"
	},
	{
	    "initiale": "وخش",
	    "finale": "ودع"
	},
	{
	    "initiale": "ودف",
	    "finale": "ورا"
	},
	{
	    "initiale": "ورث",
	    "finale": "ورد"
	},
	{
	    "initiale": "ورش",
	    "finale": "ورور"
	},
	{
	    "initiale": "وري",
	    "finale": "وزن"
	},
	{
	    "initiale": "وزوز",
	    "finale": "وسط"
	},
	{
	    "initiale": "وسع",
	    "finale": "وسع"
	},
	{
	    "initiale": "وسق",
	    "finale": "وسل"
	},
	{
	    "initiale": "وسم",
	    "finale": "وصف"
	},
	{
	    "initiale": "وصل",
	    "finale": "وصل"
	},
	{
	    "initiale": "وصي",
	    "finale": "وصي"
	},
	{
	    "initiale": "وضا",
	    "finale": "وضح"
	},
	{
	    "initiale": "وضع",
	    "finale": "وطئ"
	},
	{
	    "initiale": "وطد",
	    "finale": "وعد"
	},
	{
	    "initiale": "وعر",
	    "finale": "وعظ"
	},
	{
	    "initiale": "وعف",
	    "finale": "وفق"
	},
	{
	    "initiale": "وفق",
	    "finale": "وفق"
	},
	{
	    "initiale": "وفي",
	    "finale": "وفي"
	},
	{
	    "initiale": "وقب",
	    "finale": "وقت"
	},
	{
	    "initiale": "وقح",
	    "finale": "وقع"
	},
	{
	    "initiale": "وقف",
	    "finale": "وقف"
	},
	{
	    "initiale": "وقم",
	    "finale": "وقي"
	},
	{
	    "initiale": "وكد",
	    "finale": "وكل"
	},
	{
	    "initiale": "وكي",
	    "finale": "ولد"
	},
	{
	    "initiale": "ولد",
	    "finale": "ولد"
	},
	{
	    "initiale": "ولسس",
	    "finale": "ولم"
	},
	{
	    "initiale": "ولند",
	    "finale": "ولي"
	},
	{
	    "initiale": "ولي",
	    "finale": "ولي"
	},
	{
	    "initiale": "ولي",
	    "finale": "ولي"
	},
	{
	    "initiale": "ومن",
	    "finale": "ومن"
	},
	{
	    "initiale": "ومي",
	    "finale": "وهب"
	},
	{
	    "initiale": "وهج",
	    "finale": "ويب"
	},
	{
	    "initiale": "ويح",
	    "finale": "يبس"
	},
	{
	    "initiale": "يبن",
	    "finale": "يدي"
	},
	{
	    "initiale": "يرع",
	    "finale": "يطو"
	},
	{
	    "initiale": "يعقوب",
	    "finale": "يوليو"
	},
	{
	    "initiale": "يوس",
	    "finale": "ييس"
	}
    ]