let mask = document.getElementById("masque")
let recmasque = document.getElementById("recmasque")
let divtop = document.getElementById("top")
let img = document.getElementById("pagePrincipale")
let hdr = document.getElementById("hdr")
let inputTop = document.getElementById("inputTop")
let inputMasque = document.getElementById("inputMasque")
let retourEr = document.getElementById("retourEr")
let retourAb = document.getElementById("retourAb")
let boutonAide = document.getElementById("help")
let panneauAide = document.getElementById("panneauAide")
let pRacine = document.getElementById("pRacine")
let submitMasque = document.getElementById("submitMasque")
let pToggle = document.querySelector("#chgmt p")
let divAlphabet = document.querySelector("#voletdroit .wrap")
let aGallica = document.querySelector("#pGallica a")
let aULSA = document.querySelector("#pULSA a")
let body = document.body

const alphabet = "ابتثجحخدذرزسشصضطظعغفقكلمنهوي"
//page initiale

const posinit = Math.floor((racines.length - 1) / 2)
const onmobile = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)





class dictionnaire {
	#pageCouranteInitiale
	#entrees = ["#pageCouranteInitiale", "pageErrata", "pageAlif", "pageAbrev", "pageMin", "pageMax", "tableErrata"]
	#ULSA = [7, 781, 19, 17, 1, 791, [26, 38, 56, 74, 88, 104, 173]]
	//+4 (sauf pagemax)
	#gallica = [11, 785, 23, 21, 1, 799, [30, 42, 60, 78, 92, 108, 177]]
	#dicos = ["ULSA", "gallica"]
	//ref : de gallica vers...
	#differentiel = { "ULSA": -4, "gallica": 0 }

	//erreurs du dictionnaire à gérer...
	exceptions = { "موم": 651 }

	constructor(dict) {

		//valeur par defaut
		if (!this.#dicos.includes(dict)) dict = "gallica"

		this.#actualisationEntrees(dict)

		this.pageCourante = this.#pageCouranteInitiale
		this.pageRecherche = this.pageCourante
		this.retourabrev = 0; this.retourerrata = 0 //osef
		this.cedico = dict
		body.classList.add(this.cedico)


		//eventlisteners pour traiter la racine
		let divs = document.getElementsByClassName("racine")
		for (let i = 0; i < divs.length; i++) {
			let div = divs[i]
			let bouton = div.querySelector('.submitRacine')
			let input = div.querySelector('.inputRacine')

			input.addEventListener("input", e => {
				bouton.value = "Rechercher " + conversion(input.value)
			})

			input.addEventListener("keypress", e => {
				if (e.key == "Enter") {
					bouton.click()
				}
			})

			bouton.addEventListener("click", e => {
				//action effectuée via un clic ou une entrée appelée en un seul endroit quelle que soit la méthode
				let conv = conversion(input.value)
				if (conv == "") {
					//si la valeur d'input est invalide lors d'une soumission, on l'efface sans faire de recherche
					input.value = ""
					return;
				}

				recherche(conv)
				input.value = ""
				bouton.value = "Rechercher"
				input.blur();
				e.stopPropagation();
			})
		}

		this.actualisationPage(this.pageCourante)

		inputTop.focus()

		document.body.addEventListener("keydown", navigation)

		hdr.addEventListener("click", e => { inputTop.focus() })
		recmasque.addEventListener("click", e => { inputMasque.focus() })
		body.classList.remove("start") //pour eviter transition du background la premiere fois.


	}//fin constructeur

	chgmt(nvdico) {
		if (!this.#dicos.includes(nvdico)) return;

		//maj valeurs générales (valeur de constantes(même si pas implémentées comme telles...))
		this.#actualisationEntrees(nvdico)

		//maj valeurs courantes ( no comment )
		let diff = this.#differentiel[nvdico] - this.#differentiel[this.cedico]

		this.pageCourante = Math.min(Math.max(this.pageCourante + diff, this.pageMin), this.pageMax)
		this.retourabrev = Math.min(Math.max(this.retourabrev + diff, this.pageMin), this.pageMax)
		this.retourerrata = Math.min(Math.max(this.retourerrata + diff, this.pageMin), this.pageMax)
		this.pageRecherche = Math.min(Math.max(this.pageRecherche + diff, this.pageMin), this.pageMax)

		document.body.classList.remove(this.cedico)
		this.cedico = nvdico

		//maj affichage
		document.body.classList.add(this.cedico)
		this.actualisationPage(dico.pageCourante)

	}

	#actualisationEntrees(dico) {
		for (let [i, e] of this.#entrees.entries()) {
			eval("this." + e + "=this.#" + dico + "[i]")
		}
	}

	actualisationPage(numPage) {
		this.pageCourante = numPage
		img.src = "ressources/jpg/" + this.cedico + "/pg" + numPage + ".jpg";
		switch (this.cedico) {
			case "ULSA":
				aULSA.href = "https://menadoc.bibliothek.uni-halle.de/ssg/content/pageview/" + (1053100 + this.pageCourante)
				break;

			//gallica
			default:
				aGallica.href = "https://gallica.bnf.fr/ark:/12148/bpt6k3164649/f" + this.pageCourante + ".item"
				break;
		}

		//utile que ce soit ici pour mobile ou les errata et abrev sont sur la "page masque"
		if (body.classList.contains("actifM")) demasque()

	}


	#protation = 1;

	toggle() {
		let nvdico = this.cedico == "gallica" ? "ULSA" : "gallica"
		this.chgmt(nvdico)
		pToggle.style.rotate = this.#protation * 180 + "deg"
		this.#protation ++
	}




}








/*****************************initialisation***********************************/

const dico = new dictionnaire(dictionnaire_par_defaut);


/*
 //lol 

 let ratio = 1/(28*1.13)
 function alpha(){
	  let rect = img.getBoundingClientRect()
	  divAlphabet.style.setProperty("font-size",(window.innerHeight - Math.max(20,rect.y) - Math.max(window.innerHeight - rect.height - rect.y,20))*ratio + "px",'important')
}
 divAlphabet.style.setProperty("font-size", (window.innerHeight - Math.max(20,img.getBoundingClientRect().y) - 20)*ratio+ "px",'important')
 img.onload = (e) =>{
	  alpha()
	  window.onresize = alpha;
	  window.onscroll = alpha;
 }

 



/********************************************************************** */

function alaide() {
	hdr.classList.toggle("actif")

}

function navigation(event) {

	if (document.activeElement.tagName == "INPUT") return
	if (event.code == "ArrowLeft") {
		//eviter scroll quand présent
		event.preventDefault();
		pagePrecedente()
	} else if (event.code == "ArrowRight") {
		event.preventDefault()
		pageSuivante()
	} else if (event.code == "Space"){
		event.preventDefault()
		masque()
	}

}

//prend une racine correcte
function recherche(racine) {
	dico.actualisationPage(page(racine))
	dico.pageRecherche = dico.pageCourante
}

function pageErrataDepuisCourante() {
	let i;
	for (i = 0; dico.pageCourante > dico.tableErrata[i] && i < dico.tableErrata.length; i++) { }
	return dico.pageErrata + i
}

function errata() {
	//eviter d'annuler la page retour si clic sur errata à nouveau quand on est sur errata (supposition erreur)
	if (dico.pageCourante >= dico.pageErrata) return
	let text, pageDest = dico.pageErrata;
	if (dico.pageCourante < dico.pageAbrev) {
		text = "⤷ Retour à l'avant-propos"
	} else if (dico.pageCourante == dico.pageAbrev || dico.pageCourante == dico.pageAbrev + 1) {
		text = "⤷ Retour aux abréviations"
	} else {
		text = "⤷ Retour page " + (dico.pageCourante - dico.pageAlif + 3)
		pageDest = pageErrataDepuisCourante()
	}

	afficherRetour(retourEr, text, "errata")
	dico.actualisationPage(pageDest);

}

function abreviations() {
	//eviter d'annuler la page retour si clic sur abrev à nouveau quand on est sur abrev (supposition erreur)
	if (dico.pageCourante == dico.pageAbrev || dico.pageCourante == dico.pageAbrev + 1) return
	let text;
	if (dico.pageCourante >= dico.pageErrata) {
		text = "⤷ Retour aux errata"
	} else if (dico.pageCourante < dico.pageAbrev) {
		text = "⤷ Retour à l'avant-propos"
	} else {
		text = "⤷ Retour page " + (dico.pageCourante - dico.pageAlif + 3)
	}
	afficherRetour(retourAb, text, "abrev")
	dico.actualisationPage(dico.pageAbrev)
}

function afficherRetour(elemRetour, text, type) {
	eval("dico.retour" + type + "=dico.pageCourante")
	elemRetour.innerHTML = text
	elemRetour.style.display = "inline"
}


//type str "abrev" ou "errata"
function retourner(elemRetour, type) {

	dico.actualisationPage(eval("dico.retour" + type))
	elemRetour.style.display = "none"
}





function masque() {
	body.classList.add("actifM")
	inputMasque.focus()
	document.addEventListener("keydown", echappemasque)
}

function echappemasque(event) {


	if (event.code === "Escape") {

		demasque()
	}
}

function demasque() {
	body.classList.remove("actifM")
	inputMasque.value = ""
	submitMasque.value = "Rechercher"
	document.removeEventListener("keydown", echappemasque)
}


//convertit une racine entrée par l'utilisateur en une racine pouvant être recherchée (vaut aussi pour celles du dict vu que pas toujours été cohérent, faudra coder un truc pour faire un peu de nettoyage rapide 1 de C 4)
function conversion(input) {
	let output = input.repeat(1)
	let i
	//on recherche toutes les séquences à remplacer
	norm.forEach(element => {
		element.tableau.forEach(seq => {
			i = output.indexOf(seq)
			//tant qu'on trouve des séquences à remplacer, on les remplace (wow)
			while (i != -1) {
				output = output.substring(0, i) + element.correspondance + output.substring(i + seq.length)
				i = output.indexOf(seq)
			}
		})
	})
	//enfin, on nettoie le résultat: tous les caractères de la chaine résultante doivent être dans la constante alphabet, autrement on les retire
	for (i = output.length - 1; i > -1; i--) {
		if (!alphabet.includes(output[i])) {
			output = output.substring(0, i) + output.substring(i + 1)
		}
	}

	return output
}





//prend deux chaines de caracteres en entrée, retourne 0 si ==, un nombre >0 si requete est après dictionnaire et inversement
function comparaison(requete, dictionnaire) {
	let index = [...requete].findIndex((lettre, i) => lettre != dictionnaire[i])
	if (index === -1) {
		//dictionnaire contient requete
		return requete.length - dictionnaire.length
	} else {
		//il faut verifier que dict. n'est pas juste plus court que requete car indexOf de "" retourne 0; comme alphabet.indexOf("ا"), si jamais requete a un alif a cet endroit
		return dictionnaire[index] === "" ? 1 : alphabet.indexOf(requete[index]) - alphabet.indexOf(dictionnaire[index])
	}

}

//retourne la page souhaitée, prend en parametre une racine correcte
function page(racine, depuisRecherche = true) {

	//&nbsp
	if(depuisRecherche) pRacine.textContent = racine == "" ? "\xa0" : racine

	//gestion individualisée si le dictionnaire a placé cette racine sur la mauvaise page

	if (dico.exceptions[racine] !== undefined) {
		return dico.exceptions[racine] + dico.pageAlif
	}

	return dicht(racine, posinit, 0, racines.length - 1) + dico.pageAlif
}

function dicht(reqracine, pos, inf, sup) {

	let comp = comparaison(reqracine, conversion(racines[pos].finale))


	if (comp > 0) {
		if (pos == sup) return pos
		inf = pos + 1
		pos = Math.ceil((inf + sup) / 2)
	}
	//comp<=0
	else if (pos == 0 || comparaison(reqracine, conversion(racines[pos - 1].finale)) > 0) {
		return pos
	} else {
		sup = pos - 1
		pos = Math.floor((inf + sup) / 2)
	}


	return dicht(reqracine, pos, inf, sup)


}


function pageSuivante() {
	if (dico.pageCourante == undefined || dico.pageCourante == dico.pageMax) return;
	dico.actualisationPage(dico.pageCourante + 1)
}

function pagePrecedente() {
	if (dico.pageCourante == undefined || dico.pageCourante == dico.pageMin) return;
	dico.actualisationPage(dico.pageCourante - 1)
}
